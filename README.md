# Remove members recursively from GitLab group

Recursively remove specified users from a GitLab group, it's subgroups and projects using the GitLab API.

## Configuration

- Export / fork repository.
- Add a GitLab **group owner** API token to CI/CD variables named `GITLAB_TOKEN`. Make sure it is "masked".
This token will be used to query the API for group and project members.
The token **must** be an group owner token to be able to remove members of the group them.
- Add your GitLab instance URL to CI/CD variables named `GITLAB`.
- Configure `Config.yml` file to list all `usernames` that should be removed
- notice the Job is tagged `local` for my testing purposes. Make sure your runners pick it up.

## Usage

`python3 remove-users-recursively.py $GITLAB $GITLAB_TOKEN $GROUP $CONFIG_PATH --dryrun`

I recommend running this manually and doing a dryrun first

## Parameters

- `$GITLAB`: URL of the GitLab instance
- `$GITLAB_TOKEN`: Group owner API token
- `$GROUP`: ID or namespace of the group this script should run on
- `$CONFIG_PATH`: The path of the `Config.yml` file that lists the usernames that are to be removed
- `--dryrun`: Dryrun mode. Only compile the CSV report of users to remove, don't actually remove anyone.

## What does it do?

* Request all subgroups and projects of a group
* Get all members of the group, its subgroups and projects
  * don't get the top group owners and the owner of the API token to not lock yourself out of the group
* For all members listed in the `Config.yml` to be removed:
  * If the user is a member within the group structure, remove the user from all groups and projects they are a direct member of
* For all members to be removed:
  * Add them to a CSV report

## Disclaimer

This script is provided for educational purposes. It is **not supported by GitLab**. However, you can create an issue if you need help or propose a Merge Request. This script removes members of your group and thus their access to projects, which can be disruptive if used inappropriately. **Always run it in dryrun mode first, to understand the impact to your users.

This script produces data file artifacts containing user names. Please use this script and its outputs with caution and in accordance to your local data privacy regulations.
