#!/usr/bin/env python3

import argparse
import gitlab
from datetime import datetime
import csv
import os
import requests
import json
import yaml

def get_group_users(gl, topgroup):
    grouplist = [topgroup]
    projects = []
    users = {}
    get_sub_groups(gl, topgroup, grouplist)
    for group in grouplist:
        get_projects(gl, group, projects)
        group_users = group.members.list(as_list=False)
        for group_user in group_users:
            # topgroup owner, don't remove so they don't lock themselves out
            if group.id == topgroup.id and group_user.attributes["access_level"] == 50:
                continue
            if group_user.id not in users:
                users[group_user.id] = gl.users.get(group_user.id).attributes
                users[group_user.id]["groups"] = [group.attributes["full_path"]]
                users[group_user.id]["projects"] = []
            else:
                users[group_user.id]["groups"].append(group.attributes["full_path"])
    for project in projects:
        project_users = project.members.list(as_list=False)
        for project_user in project_users:
            if project_user.id not in users:
                users[project_user.id] = gl.users.get(project_user.id).attributes
                users[project_user.id]["groups"] = []
                users[project_user.id]["projects"] = [project.attributes["path_with_namespace"]]
            else:
                users[project_user.id]["projects"].append(project.attributes["path_with_namespace"])
    return users

def remove_current_user(gl, users):
    gl.auth()
    current_user = gl.user
    del users[current_user.id]

def get_sub_groups(gl, group, grouplist):
    groups = group.subgroups.list(as_list=False)
    for subgroup in groups:
        subgroup_object = gl.groups.get(subgroup.id)
        grouplist.append(subgroup_object)
        get_sub_groups(gl, subgroup_object, grouplist)

def get_projects(gl, group, projectlist):
    projects = group.projects.list(as_list=False)
    for project in projects:
        projectlist.append(gl.projects.get(project.id))
    return projectlist

def get_last_user_events(gl, users):
    last_events = {}
    for user in users.values():
        user_object = gl.users.get(user["id"], lazy = True)
        userevents = user_object.events.list(as_list = False)
        try:
            last_event = userevents.next()
            if last_event:
                last_events[user["id"]] = last_event.attributes["created_at"]
        except:
            print("No activity for %s" % user["username"])
    return last_events

def get_users_to_remove(users, usernames):
    removelist = []
    for user in users.values():
        remove = False
        if user["username"] in usernames:
            remove = True
        if remove:
            removelist.append(user)
    return removelist

def remove_users(gl, users):
    '''
    iterate through all groups and projects and remove.
    python-gitlab already does the recursive remove, so catch all exceptions well and make sure delete really did delete
    '''

    for user in users:
        for group in user["groups"]:
            group_object = gl.groups.get(group, lazy = True)
            try:
                print("Removing %s from %s" % (user["username"], group))
                group_object.members.delete(user["id"])
            except:
                try:
                    still_in_group = group_object.members.get(user["id"])
                    print("Could not remove %s from %s" % (user["username"], group))
                except gitlab.exceptions.GitlabGetError as e:
                    if "404" in str(e):
                        pass
                    else:
                        print("Could not remove %s from %s" % (user["username"], group))

        for project in user["projects"]:
            project_object = gl.projects.get(project, lazy = True)
            try:
                print("Removing %s from %s" % (user["username"], project))
                project_object.members.delete(user["id"])
            except:
                try:
                    still_in_project = project_object.members.get(user["id"])
                except gitlab.exceptions.GitlabGetError as e:
                    if "404" in str(e):
                        pass
                    else:
                        print("Could not remove %s from %s" % (user["username"], project))

def write_report(reportfilepath, users_to_remove):
    with open(reportfilepath, "w") as reportfile:
        reportwriter = csv.writer(reportfile, delimiter="\t", quotechar='"', quoting=csv.QUOTE_MINIMAL)
        fields = ["username","name","groups","projects"]
        reportwriter.writerow(fields)
        for user in users_to_remove:
            row = []
            for field in fields:
                if field == "groups" or field == "projects":
                    row.append(user[field])
                else:
                    row.append(user[field])
            reportwriter.writerow(row)


parser = argparse.ArgumentParser(description='Recursively remove group members')
parser.add_argument('gitlaburl', help='Url of the gitlab instance')
parser.add_argument('token', help='API token able to read the requested projects')
parser.add_argument('group', help='ID or namespace of the group in which to remove members')
parser.add_argument('configfile', help='The YAML file the defines the users to remove')
parser.add_argument('--dryrun', help='Only output list of users that would be removed without actually removing', action="store_true")
args = parser.parse_args()

users = []

configfile = args.configfile
with open(configfile, "r") as c:
    config = yaml.load(c, Loader=yaml.FullLoader)
    if "usernames" in config:
        usernames = config["usernames"]
    else:
        print("Error: No users configured for removal. Stopping.")
        exit(1)

gitlaburl = args.gitlaburl if args.gitlaburl.endswith("/") else args.gitlaburl + "/"
gl = gitlab.Gitlab(gitlaburl, private_token = args.token)
headers = {'PRIVATE-TOKEN': args.token}

do_dryrun = args.dryrun

try:
    group = gl.groups.get(args.group)
except Exception as e:
    print("Can not retrieve list of users: "+ str(e))
    exit(1)

# Get all users in the group specified
users = get_group_users(gl, group)
# make sure we don't accidentally remove ourselves from anything
remove_current_user(gl, users)

users_to_remove = get_users_to_remove(users, usernames)

if not do_dryrun:
    remove_users(gl, users_to_remove)
    print("Removed %s users, writing report." % str(len(users_to_remove)))
else:
    print("There are %s users to remove, writing report." % str(len(users_to_remove)))

reportfilepath = "report/removed_users_%s.csv" % str(datetime.now().date())
if do_dryrun:
    reportfilepath = "report/users_to_be_removed_%s.csv" % str(datetime.now().date())
os.makedirs(os.path.dirname(reportfilepath), exist_ok=True)

write_report(reportfilepath, users_to_remove)

